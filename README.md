# Google Cloud Platform (GCP)
## Install oogle-cloud-sdk
* [Download](https://cloud.google.com/sdk/docs/install)
* unzip
* ./google-cloud-sdk/install.sh
* gcloud init

## use gcloud ssh
* gcloud compute ssh example-instance

## use normal ssh
* [參考](https://medium.com/%E5%B7%A5%E7%A8%8B%E9%9A%A8%E5%AF%AB%E7%AD%86%E8%A8%98/%E5%A6%82%E4%BD%95%E5%BB%BA%E7%AB%8B%E9%81%A0%E7%AB%AFvscode%E7%92%B0%E5%A2%83-8294c813b453)
* cd ~
* ssh-keygen
* cat ~/.ssh/id_rsa.pub
* 打開 Compute Engine 的中繼資料
* 點擊安全殼層金鑰，把公鑰id_rsa.pub的內容貼進去就完成了